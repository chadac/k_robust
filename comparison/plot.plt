# set term pdf
set term x11

k = 3
n = 100

# do for [k=3:5] {
# do for [n=50:200:50] {

set output sprintf('pc0.25/pc0.25_n%d_k%d.pdf', n, k)

set style fill solid 0.25 border 0
set style boxplot outliers pointtype 7
set style data boxplot
set boxwidth  1
set pointsize 0.5


set border 2
set xtics (50, 75, 100, 125, 150, 175, 200)
# set xtics nomirror
# set ytics nomirror
set xrange [35:210]
unset yrange

set title sprintf("Average cost of proportional cost dataset with %d agents, k = %d", n, k)
set xlabel "# of Tasks"
set ylabel "Cost"

set key right bottom

# plot 'results_std0.2500.txt' u 3:($2 == n && $4 == k ? $5:1/0) title "NSGA-II"

     # sprintf('heuristic/plottttresultmu_data_p2std_0.2500ut20k_test2_%d.xls',k) u 2:($4 == n ? $12:1/0) smooth bezier title "Heuristic 1" lc 2,\
     # sprintf('heuristic/plottttresultmu_data_p2std_0.2500ut20k_test2_%d.xls',k) u 2:($4 == n ? $12:1/0) smooth bezier title "Heuristic 2" lc 3,\
     # sprintf('heuristic/plottttresultmu_data_p2std_0.2500ut20k_test2_%d.xls',k) u 2:($4 == n ? $12:1/0) smooth bezier title "Heuristic 3" lc 5
     # sprintf('zenefa/plottttresult_p2std_0.2500ut20k_%d.xls',k) u 2:($4 == n ? $12:1/0) smooth bezier title "Linear Program" lc 4

plot 'results_std0.2500.txt' u ($3-10):($3 == 50 && $2 == 100 && $4 == 3 ? $5:1/0) title "NSGA-II" lc 1,\
     ''  u ($3-10):($3 ==  75 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     ''  u ($3-10):($3 == 100 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     ''  u ($3-10):($3 == 125 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     ''  u ($3-10):($3 == 150 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     ''  u ($3-10):($3 == 175 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     ''  u ($3-10):($3 == 200 && $2 == n && $4 == k ? $5:1/0) notitle lc 1
     # sprintf('heuristic/plottttresultmu_data_p2std_0.2500ut20k_test2_%d.xls',k) u ($2-5):($2 == 50 && $4 == 100 ? $12:1/0) title "Heuristic 1" lc 2,\
     # '' u ($2-5):($2 ==  75 && $4 == n ? $6:1/0) notitle lc 2,\
     # '' u ($2-5):($2 == 100 && $4 == n ? $6:1/0) notitle lc 2,\
     # '' u ($2-5):($2 == 125 && $4 == n ? $6:1/0) notitle lc 2,\
     # '' u ($2-5):($2 == 150 && $4 == n ? $6:1/0) notitle lc 2,\
     # '' u ($2-5):($2 == 175 && $4 == n ? $6:1/0) notitle lc 2,\
     # '' u ($2-5):($2 == 200 && $4 == n ? $6:1/0) notitle lc 2,\
     # sprintf('heuristic/plottttresultmu_data_p2std_0.2500ut20k_test2_%d.xls',k) u ($2):($2 == 50 && $4 == 100 ? $12:1/0) title "Heuristic 2" lc 3,\
     # '' u ($2):($2 ==  75 && $4 == n ? $8:1/0) notitle lc 3,\
     # '' u ($2):($2 == 100 && $4 == n ? $8:1/0) notitle lc 3,\
     # '' u ($2):($2 == 125 && $4 == n ? $8:1/0) notitle lc 3,\
     # '' u ($2):($2 == 150 && $4 == n ? $8:1/0) notitle lc 3,\
     # '' u ($2):($2 == 175 && $4 == n ? $8:1/0) notitle lc 3,\
     # '' u ($2):($2 == 200 && $4 == n ? $8:1/0) notitle lc 3,\
     # sprintf('heuristic/plottttresultmu_data_p2std_0.2500ut20k_test2_%d.xls',k) u ($2+5):($2 == 50 && $4 == 100 ? $12:1/0) title "Heuristic 3" lc 5,\
     # '' u ($2+5):($2 ==  75 && $4 == n ? $10:1/0) notitle lc 5,\
     # '' u ($2+5):($2 == 100 && $4 == n ? $10:1/0) notitle lc 5,\
     # '' u ($2+5):($2 == 125 && $4 == n ? $10:1/0) notitle lc 5,\
     # '' u ($2+5):($2 == 150 && $4 == n ? $10:1/0) notitle lc 5,\
     # '' u ($2+5):($2 == 175 && $4 == n ? $10:1/0) notitle lc 5,\
     # '' u ($2+5):($2 == 200 && $4 == n ? $10:1/0) notitle lc 5,\
     # sprintf('zenefa/plottttresult_p2std_0.2500ut20k_%d.xls',k) u ($2+10):($2 == 50 && $4 == 100 ? $12:1/0) title "Linear Program" lc 4,\
     # '' u ($2+10):($2 ==  75 && $4 == n ? $6:1/0) notitle lc 4,\
     # '' u ($2+10):($2 == 100 && $4 == n ? $6:1/0) notitle lc 4,\
     # '' u ($2+10):($2 == 125 && $4 == n ? $6:1/0) notitle lc 4,\
     # '' u ($2+10):($2 == 150 && $4 == n ? $6:1/0) notitle lc 4,\
     # '' u ($2+10):($2 == 175 && $4 == n ? $6:1/0) notitle lc 4,\
     # '' u ($2+10):($2 == 200 && $4 == n ? $6:1/0) notitle lc 4

unset output

# }}