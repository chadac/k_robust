set term pdf monochrome

# k = 5
# n = 90

do for [k=3:5] {
do for [n=30:110:30] {

set output sprintf('ncnt/norm_cost_norm_task_n%d_k%d.pdf', n, k)

set style fill solid 0.25 border 0
set style boxplot outliers pointtype 7
set style data boxplot
set boxwidth  3
set pointsize 0.5

set border 2
set xtics (20, 50, 80, 110)
# set xtics nomirror
# set ytics nomirror
set xrange [5:135]

set title sprintf("Average cost of normal cost, normal task dataset with %d agents, k = %d", n, k)

set xlabel "# of Tasks"
set ylabel "Cost"

set key right bottom

plot\
    'nc_nt.csv' u ($3-10):($3 == 20 && $2 == n && $4 == k ? $5:1/0) title "NSGA-II" lc 1,\
     ''  u ($3-10):($3 == 50 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     ''  u ($3-10):($3 == 80 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     ''  u ($3-10):($3 == 110 && $2 == n && $4 == k ? $5:1/0) notitle lc 1,\
     sprintf('zenefa/plottttresult_normal_cost_normal_task_%d.xls',k) u ($2-5):($2 == 20 && $4 == n ? $12:1/0) title "Heuristic 1" lc 2,\
     '' u ($2-5):($2 ==  50 && $4 == n ? $6:1/0) notitle lc 2,\
     '' u ($2-5):($2 ==  80 && $4 == n ? $6:1/0) notitle lc 2,\
     '' u ($2-5):($2 == 110 && $4 == n ? $6:1/0) notitle lc 2,\
     sprintf('zenefa/plottttresult_normal_cost_normal_task_%d.xls',k) u ($2):($2 == 20 && $4 == n ? $12:1/0) title "Heuristic 2" lc 3,\
     '' u ($2):($2 ==  50 && $4 == n ? $8:1/0) notitle lc 3,\
     '' u ($2):($2 ==  80 && $4 == n ? $8:1/0) notitle lc 3,\
     '' u ($2):($2 == 110 && $4 == n ? $8:1/0) notitle lc 3,\
     sprintf('zenefa/plottttresult_normal_cost_normal_task_%d.xls',k) u ($2+5):($2 == 20 && $4 == n ? $12:1/0) title "Heuristic 3" lc 5,\
     '' u ($2+5):($2 ==  50 && $4 == n ? $10:1/0) notitle lc 5,\
     '' u ($2+5):($2 ==  80 && $4 == n ? $10:1/0) notitle lc 5,\
     '' u ($2+5):($2 == 110 && $4 == n ? $10:1/0) notitle lc 5,\
     sprintf('zenefa/plottttresult_normal_cost_normal_task_%d.xls',k) u ($2+10):($2 == 20 && $4 == n ? $12:1/0) title "Linear Program" lc 4,\
     '' u ($2+10):($2 ==  50 && $4 == n ? $12:1/0) notitle lc 4,\
     '' u ($2+10):($2 ==  80 && $4 == n ? $12:1/0) notitle lc 4,\
     '' u ($2+10):($2 == 110 && $4 == n ? $12:1/0) notitle lc 4

unset output

}}