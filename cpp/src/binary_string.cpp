#include "binary_string.h"

#include <cmath>
#include <cstring>

#include "evaluate.h"

char* BinaryString::get_a() {
  return a;
}
int BinaryString::get_length() {
  return length;
}

BinaryString::BinaryString(char* a): Chromosome(2), a(a) {}

double* BinaryString::calc_fitness() {
  double *f = new double[3];
  // cout << a << "\n";
  // print_a(a);
  f[0] = (double)k_robust(a);
  f[1] = MAX_COST - cost(a);
  // f[2] = (double)(MAX_K - max_k(a));
  return f;
}
// double BinaryString::calc_cfitness() {
//   double* f = fitness();
//   double k = f[0];
//   double c = f[1];
//   return log(c) + k;
// }

unordered_map<string,BinaryString*>* BinaryString::chmap = new unordered_map<string,BinaryString*>();

BinaryString* BinaryString::get_instance(char* a) {
  string s = string(a, A);
  if(chmap->find(s) == chmap->end()) {
    BinaryString *bs = new BinaryString(a);
    chmap->insert(make_pair(s,bs));
  }
  return chmap->at(s);
  // return new BinaryString(a);
}

void BinaryString::initialize() {
  chmap->reserve(1000 * 1000);
  chmap->max_load_factor(0.8);
}
