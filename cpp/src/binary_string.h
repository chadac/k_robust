#ifndef binary_string_h
#define binary_string_h

#include <iostream>
#include <unordered_map>
#include <string>
using namespace std;

#include "chromosome.h"

struct BStringA {
  char* a;
  char* b;
  BStringA(char* a);
};

class BinaryString: public Chromosome {
public:
  static BinaryString* get_instance(char* a);
  static void initialize();

  char* get_a();
  int get_length();
protected:
  double* calc_fitness();
  /* double calc_cfitness(); */

private:
  BinaryString(char* a);
  char* a;
  int length;
  static unordered_map<string,BinaryString*> *chmap;
};

#endif
