#include "chromosome.h"

double* Chromosome::fitness() {
  if(!vfit) vfit = calc_fitness();
  return vfit;
}
// double Chromosome::cfitness() {
//   if(!vcfit) vcfit = calc_cfitness();
//   return vcfit;
// }

bool Chromosome::operator<(Chromosome& b) {
  double *f1 = fitness(), *f2 = b.fitness();
  bool equal = true;
  for(int j = 0; j < NUM_OBJECTIVES; j++) {
    if(f1[j] < f2[j]) {
      return false;
    }
    else if(f1[j] > f2[j]) {
      equal = false;
    }
  }
  return !equal;
}

double* Chromosome::calc_fitness() { return 0; }
// double Chromosome::calc_cfitness() { return 0; }

Chromosome::Chromosome(int NUM_OBJECTIVES): NUM_OBJECTIVES(NUM_OBJECTIVES), vfit(0) {}
