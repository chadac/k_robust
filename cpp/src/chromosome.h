#ifndef chromosome_h
#define chromosome_h

class Chromosome {
public:
  double* fitness();
  /* /\* Cumulative fitness function *\/ */
  /* double cfitness(); */

  const int NUM_OBJECTIVES; /* The number of objectives that the fitness function returns */
  bool operator<(Chromosome& b);
protected:
  Chromosome(int NUM_OBJECTIVES);
  virtual double* calc_fitness();
private:
  double* vfit;			// Stored fitness value
  /* double vcfit; */
};

#endif
