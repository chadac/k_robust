#include "crossover.h"

#include <stdlib.h>
#include <math.h>
#include <cstring>
// #include <stdlib.h>

char* bstring_uniform(char* a, char* b, int length) {
  char* c = new char[length];
  for(int i = 0; i < length; i++) {
    c[i] = rand() % 2 ? a[i]:b[i];
  }
  return c;
}

char* bstring_split(char* a, char* b, int length) {
  char* c = new char[length];
  int s = rand()%length;
  for(int i = 0; i < length; i++) {
    c[i] = i > s ? a[i]:b[i];
  }
  return c;
}

bool contains(char* a, char f, int length) {
  for(int i = 0; i < length; i++) {
    if(a[i] == f) return true;
  }
  return false;
}

int find(char* a, char item, int length) {
  for(int i = 0; i < length; i++) {
    if(a[i] == item) return i;
  }
  return -1;
}

void pmx_fix(char* a, char* b, char* c, int length, int l, int r, int i, char item) {
  int j = find(b, a[i], length);
  if(l <= j && j < r) pmx_fix(a, b, c, length, l, r, j, item);
  else c[j] = item;
}

char* permute_pmx(char* a, char* b, int length) {
  int r1 = rand() % length, r2 = rand() % length;
  char* c = new char[length];
  // for(int i = 0; i < length; i++) c[i] = b[i];
  std::memcpy(c, b, length*sizeof(char));
  if(r1 > r2) {
    int tmp = r1;
    r1 = r2; r2 = tmp;
  }
  // for(int i = r1; i < r2; i++) c[i] = a[i];
  std::memcpy(&c[r1], &a[r1], (r2-r1)*sizeof(char));
  for(int i = r1; i < r2; i++) {
    if(contains(&a[i], b[i], r2-r1))
      continue;
    pmx_fix(a, b, c, length, r1, r2, i, b[i]);
  }

  return c;
}
