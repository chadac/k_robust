#ifndef crossover_h
#define crossover_h

char* bstring_uniform(char* a, char* b, int length);
char* bstring_split(char* a, char* b, int length);

char* permute_pmx(char* a, char* b, int length);

#endif
