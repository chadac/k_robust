#include "evaluate.h"

// #ifdef DEFAULT_KROBUST_CONFIG

// vector<int> *init_alpha() {
//   vector<int> *alpha = new vector<int>[10];
//   for(int i = 0; i < 10; i++) 
//     alpha[i] = vector<int>();
//   int a0[] = {0,1};
//   alpha[0].assign(a0, a0+2);
//   int a1[] = {4,5};
//   alpha[1].assign(a1, a1+2);
//   int a2[] = {8,9};
//   alpha[2].assign(a2, a2+2);
//   int a3[] = {1,2,3,4};
//   alpha[3].assign(a3, a3+4);
//   int a4[] = {5,6,7,8};
//   alpha[4].assign(a4, a4+4);
//   int a5[] = {0,9};
//   alpha[5].assign(a5, a5+2);
//   int a6[] = {0,1};
//   alpha[6].assign(a6, a6+2);
//   int a7[] = {0,3,6,9};
//   alpha[7].assign(a7, a7+4);
//   int a8[] = {0,1,2,3,4};
//   alpha[8].assign(a8, a8+5);
//   int a9[] = {5,6,7,8,9};
//   alpha[9].assign(a9, a9+5);
//   return alpha;
// }

// unsigned int A = 10;
// unsigned int T = 10;
// int* f = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
// vector<int> *alpha = init_alpha();

// int MAX_COST;
// int MAX_K;

// #else

unsigned int A;
unsigned int T;
double* f;
vector<int> *alpha;

double MAX_COST;
int MAX_K;

// #endif

void calc_max() {
  MAX_COST = 0;
  for(unsigned int i = 0; i < A; i++) MAX_COST += f[i];

  char a[A];
  fill(&a[0],&a[A-1],1);
  MAX_K = k_robust(a);
}


/**
 * Returns the maximal k value of the given assignment
 **/
int max_k(char* a) {
  int *k = new int[T];
  for(unsigned int i = 0; i < T; i++) k[i] = 0;
  for(unsigned int i = 0; i < A; i++) {
    if(!a[i]) continue;
    for(unsigned int j = 0; j < alpha[i].size(); j++) {
      ++k[alpha[i].at(j)];
    }
  }
  int max_k = A+1;
  for(unsigned int j = 0; j < T; j++) {
    max_k = max(max_k, k[j]);
  }
  return max_k;
}

/**
 * Returns the k-robustness of the given set.
 **/
int k_robust(char* a) {
  int *k = new int[T];
  for(unsigned int i = 0; i < T; i++) k[i] = 0;
  for(unsigned int i = 0; i < A; i++) {
    if(!a[i]) continue;
    for(unsigned int j = 0; j < alpha[i].size(); j++) {
      ++k[alpha[i].at(j)];
    }
  }
  int min_k = A+1;
  for(unsigned int j = 0; j < T; j++) {
    min_k = min(min_k, k[j]);
  }
  return min_k;
}

/**
 * Returns the cost of the given set.
 **/
double cost(char* a) {
  double cost = 0;
  for(unsigned int i = 0; i < A; i++) {
    cost += a[i] ? f[i] : 0;
  }
  return cost;
}

void print_a(char* a) {
  for(unsigned int i = 0; i < A; i++) {
    cout << (a[i] ? "1":"0");
  }
  cout << "\n";
}
