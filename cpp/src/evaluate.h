#ifndef evaluate_h
#define evaluate_h
/* #define DEFAULT_KROBUST_CONFIG */

#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

extern unsigned int A;
extern unsigned int T;
extern double* f;
extern vector<int> *alpha;

extern double MAX_COST;
extern int MAX_K;

void calc_max();

int k_robust(char* a);
double cost(char* a);
int max_k(char* a);

void print_a(char* a);

#endif
