#include "ga.h"

#include <cstdlib>

GeneticAlgorithm::GeneticAlgorithm() {
}

void GeneticAlgorithm::init_population() {}
Chromosome* GeneticAlgorithm::select(Chromosome** pool, int length) { return 0; }
Chromosome* GeneticAlgorithm::cross(Chromosome &a, Chromosome &b) { return 0; }
Chromosome* GeneticAlgorithm::mutate(Chromosome &a) { return 0; }

void GeneticAlgorithm::run_ga() {
  init_population();
  
  for(int g = 1; g <= NUM_GENERATIONS; g++) {
    run_generation();
  }
}

void GeneticAlgorithm::run_generation() {  
  // GA junk
  Chromosome* child_pool[POOL_SIZE];
  for(int i = 0; i < POOL_SIZE; i++) {
    child_pool[i] = select(pool, POOL_SIZE);
  }

  Chromosome* children[POOL_SIZE];
  for(int i = 0; i < POOL_SIZE; i++) {
    int r1 = rand() % POOL_SIZE;
    int r2 = rand() % POOL_SIZE;
    if(rand() % 100000 < CROSS_PROB * 100000) {
      children[i] = cross(*child_pool[r1], *child_pool[r2]);
    }
    else {
      children[i] = child_pool[r1];
    }
    
    if(rand() % 100000 < MUT_PROB * 100000) {
      children[i] = mutate(*children[i]);
    }
  }
  
  for(int i = 0; i < POOL_SIZE; i++) {
    pool[i] = children[i];
  }
}

int GeneticAlgorithm::get_pool_size() {
  return POOL_SIZE;
}
