#ifndef GA_H
#define GA_H

#include "chromosome.h"

class GeneticAlgorithm {
public:
  GeneticAlgorithm();
  
  /**
   * Initializes the population.
   **/
  virtual void init_population();
  /**
   * Returns the array of chromosomes to select from.
   **/
  virtual Chromosome* select(Chromosome** pool, int length);
  /**
   * Returns the crossing of two chromosomes (not destructive).
   **/
  virtual Chromosome* cross(Chromosome &a, Chromosome &b);
  /**
   * Mutates a chromosome (not destructive).
   **/
  virtual Chromosome* mutate(Chromosome &a);

  /**
   * Runs the genetic algorithm for the specified parameters.
   **/
  void run_ga();

  /**
   * Runs a single generation.
   **/
  void run_generation();
  
  /**
   * Returns the kth "best" chromosome, although that's actually a
   * lie.
   **/
  Chromosome* get_chromosome(int k);

  int get_pool_size();

protected:
  Chromosome** pool;
  int POOL_SIZE;

private:
  double CROSS_PROB;		// Probability of crossover
  double MUT_PROB;		// Probability of mutation
  int NUM_GENERATIONS;		// Time to run the GA for
};

#endif
