#include "krobustga.h"
#include <algorithm>

KRobustGA::KRobustGA(): MultipleObjectiveGA(2) {
  BinaryString::initialize();
  
  fb = new double*[3];
  fb[0] = new double[2];
  fb[0][0] = 0;
  fb[0][1] = MAX_K;
  fb[1] = new double[2];
  fb[1][0] = 0;
  fb[1][1] = MAX_COST;
  fb[2] = new double[2];
  fb[1][0] = 0;
  fb[1][1] = MAX_K;
}

void KRobustGA::init_population() {
  unsigned int n = CHILD_POOL_SIZE+PARENT_POOL_SIZE;
  for(unsigned int i = 0; i < n; i++) {
    char *c = new char[A];
    if(i == 0) {
      fill(c, c+A, 0);
    }
    else if(i == 1) {
      fill(c, c+A, 1);
    }
    else {
      for(unsigned int j = 0; j < A; j++) {
	c[j] = rand() % 2 ? 1:0;
      }
    }
    pool[i] = BinaryString::get_instance(c);
  }
}

Chromosome* KRobustGA::select(W** pool, int length) {
  return tournament(pool, length, 0.65);
  // return rank(pool, length);
}

Chromosome* KRobustGA::cross(Chromosome &a, Chromosome &b) {
  BinaryString* bsa = (BinaryString*) &a;
  BinaryString* bsb = (BinaryString*) &b;
  char* c = bstring_uniform(bsa->get_a(), 
			    bsb->get_a(),
			    A);
  return BinaryString::get_instance(c);
}

Chromosome* KRobustGA::mutate(Chromosome &a) {
  BinaryString* bsa = (BinaryString*) &a;
  char* c = bstring_bit(bsa->get_a(), A);
  return BinaryString::get_instance(c);
}
