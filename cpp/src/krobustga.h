#ifndef krobustga_h
#define krobustga_h

#include "nsga.h"
#include "binary_string.h"
#include "evaluate.h"
#include "select.h"
#include "crossover.h"
#include "mutate.h"

class KRobustGA: public MultipleObjectiveGA {
public:
  KRobustGA();

  void init_population();
  Chromosome* select(W** pool, int length);
  Chromosome* cross(Chromosome &a, Chromosome &b);
  Chromosome* mutate(Chromosome &a);
};

#endif
