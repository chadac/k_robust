#include "krobustga.h"
#include "evaluate.h"
#include "binary_string.h"

#include "read_dataset.h"
#include "crossover.h"

#include <cstdio>
using namespace std;

int main(int argc, char** argv) {
  char* filename;
  if(!argv[1]) {
    filename = "../datasets/uniform_cost_uniform_task/00030n_00050m_001t.json.txt";
  }
  else filename = argv[1];
  read_dataset(filename);
  calc_max();

  KRobustGA *ga = new KRobustGA();
  ga->run_ga();

  double best_c[MAX_K+1];
  for(int i = 0; i <= MAX_K; i++) best_c[i] = MAX_COST;
  for(int t = 0; t < ga->get_pool_size(); t++) {
    Chromosome* bch = ga->get_chromosome(t);
    char* a = ((BinaryString*) bch)->get_a();
    int k = k_robust(a);
    float c = cost(a);
    if(c < best_c[k]) best_c[k] = c;
  }

  for(int k = 0; k <= MAX_K; k++) {
    printf("%30s %8d %8d %5d %10.2f\n", filename, A, T, k, best_c[k]);
  }

  return 0;
}
