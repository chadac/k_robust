#include "mutate.h"

#include <cstring>
#include <stdlib.h>

char* bstring_bit(char* a, unsigned int length) {
  char* b = new char[length];
  std::memcpy(b, a, length*sizeof(char));
  int r = rand()%length;
  b[r] = (b[r] ? 0:1);
  return b;
}

char* permute_swap(char* a, unsigned int length) {
  char* b = new char[length];
  std::memcpy(b, a, length*sizeof(char));
  int r1 = rand()%length, r2 = rand()%length;
  char tmp = b[r1];
  b[r1] = b[r2];
  b[r2] = tmp;
  return b;
}
