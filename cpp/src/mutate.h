#ifndef mutate_h
#define mutate_h

char* bstring_bit(char* a, unsigned int length);

char* permute_swap(char* a, unsigned int length);

#endif
