#include "nsga.h"

#include <iostream>

W::W(Chromosome* c, double w): c(c), w(w) {
  f = c->fitness();
  rank = 1;
  order = 0;
  n = 0;
}
W::~W() {
  delete[] f;
}

bool W::operator<(W &b) {
  return (rank < b.rank) || (rank == b.rank && this->w > b.w);
}

// Returns the length of a null-terminated array
int null_len(Chromosome* f) {
  int* l = (int*)f;
  while(l) l++;
  return (int)(l - (int*)f);
}

MultipleObjectiveGA::MultipleObjectiveGA(int NUM_OBJECTIVES):
  NUM_OBJECTIVES(NUM_OBJECTIVES)
{
  CROSS_PROB = 0.95;
  MUT_PROB = 0.10;
  PARENT_POOL_SIZE = 75; //100
  CHILD_POOL_SIZE = 150; //200
  NUM_GENERATIONS = 1500; //2000

  pool = new Chromosome*[PARENT_POOL_SIZE+CHILD_POOL_SIZE];
}

void MultipleObjectiveGA::init_population() {}
Chromosome* MultipleObjectiveGA::select(W** pool, int length) {return 0;}
Chromosome* MultipleObjectiveGA::cross(Chromosome &a, Chromosome &b) {return 0;}
Chromosome* MultipleObjectiveGA::mutate(Chromosome &a) {return 0;}

bool MultipleObjectiveGA::dominates(Chromosome &a, Chromosome &b) {
  double *f1 = a.fitness(), *f2 = b.fitness();
  bool equal = true;
  for(int j = 0; j < NUM_OBJECTIVES; j++) {
    if(f1[j] < f2[j]) {
      return false;
    }
    else if(f1[j] > f2[j]) {
      equal = false;
    }
  }
  return !equal;
}

void MultipleObjectiveGA::fast_non_dominated_sort(W** I) {
  unsigned int n = PARENT_POOL_SIZE+CHILD_POOL_SIZE;
  W *I2[n+1];
  I2[n] = 0;
  W **f1 = I2, **f2 = I2;
  unsigned int G[n];
  unsigned int *g1 = G;
  unsigned int *g2 = G;
  vector<int> S[n];
  for(unsigned int i = 0; i < n; i++) {
    // S[i] = vector<int>();
    G[i] = 0;
    I2[i] = (W*)1;
  }
  for(unsigned int i = 0; i < n; i++) {
    W* p = I[i];
    for(unsigned int j = 0; j < n; j++) {
      if(i == j) continue;
      W* q = I[j];
      if(dominates(*p->c, *q->c)) {
	S[i].push_back(j);
      }
      else if(dominates(*q->c, *p->c)) p->n++;
    }
    if(!p->n) {
      p->rank = 1;
      *f2 = p; *g2 = i;
      f2++; g2++;
      if(!*f2) goto endloop1;
    }
  }

  do {
    int crank = (*f1)->rank;
    while((*f1)->rank == crank) {
      int i = *g1;
      for(unsigned int c = 0; c < S[i].size(); c++) {
	int j = S[i][c];
	W* q = I[j];
	q->n--;
	if(!q->n) {
	  q->rank = crank+1;
	  *f2 = I[j]; *g2 = j;
	  // cout << f2 << *g2 << "\n";
	  f2++; g2++;
	  if(!*f2) goto endloop1;
	}
      }
      f1++; g1++;
    }
    // *f2 = 0;
    // f2++; g2++;
    // f1++; g1++;
  } while(f2);
 endloop1:
  // cout << (f2-I2) << "\n";
  for(unsigned int i = 0; i < n; i++) {
    I[i] = I2[i];
  }
}

struct Comparator {
  int j;
  Comparator(int j): j(j) {}
  
  bool operator() (const W* a, const W* b) const {
    return a->f[j] < b->f[j];
  }
};

void MultipleObjectiveGA::crowding_distance_assignment(W** I, int n) {
  // Update weights
  for(int j = 0; j < NUM_OBJECTIVES; j++) {
    sort(&I[0], &I[n-1], Comparator(j));
    I[0]->w = INFINITY;
    I[n-1]->w = INFINITY;
    for(int i = 1; i < n-1; i++) {
      I[i]->w += (I[i+1]->f[j] - I[i-1]->f[j])/(fb[j][1] - fb[j][0]);
    }
  }
  return;
}

void MultipleObjectiveGA::run_ga() {
  // printf("Init population...\n");
  init_population();
  
  for(int g = 1; g <= NUM_GENERATIONS; g++) {
    // printf("Generation %d\n", g);
    run_generation();
  }
}

Chromosome* MultipleObjectiveGA::get_chromosome(int k) {
  return pool[k];
}

struct Comparator2 {
  bool operator() (const W* a, const W* b) const {
    return (a->rank < b->rank) || (a->rank == b->rank && a->w > b->w);
  }
};

void MultipleObjectiveGA::run_generation() {
  int n = PARENT_POOL_SIZE+CHILD_POOL_SIZE;
  W* I[n];

  for(int i = 0; i < n; i++) {
    I[i] = new W(pool[i], 0.0);
  }
  fast_non_dominated_sort(I);
  int l = 0, r = 0;

  int rank = 0;
  while(l < n) {
    rank = I[l]->rank;
    while(r < n && I[r++]->rank == rank);
    crowding_distance_assignment(&I[l], r-l);
    l = r;
  }
  sort(I, &I[n], Comparator2());
  
  // GA junk
  Chromosome* child_pool[CHILD_POOL_SIZE];

  for(int i = 0; i < PARENT_POOL_SIZE; i++) {
    pool[i] = I[i]->c;
  }
  for(int i = 0; i < CHILD_POOL_SIZE; i++) {
    child_pool[i] = select(I, PARENT_POOL_SIZE);
  }

  Chromosome* children[CHILD_POOL_SIZE];
  for(int i = 0; i < CHILD_POOL_SIZE; i++) {
    int r1 = rand() % CHILD_POOL_SIZE;
    int r2 = rand() % CHILD_POOL_SIZE;
    if(rand() % 100000 < CROSS_PROB * 100000) {
      children[i] = cross(*child_pool[r1], *child_pool[r2]);
    }
    else {
      children[i] = child_pool[r1];
    }
    
    if(rand() % 100000 < MUT_PROB * 100000) {
      children[i] = mutate(*children[i]);
    }
  }
  
  for(int i = 0; i < CHILD_POOL_SIZE; i++) {
    pool[PARENT_POOL_SIZE+i] = children[i];
  }
}

int MultipleObjectiveGA::get_pool_size() {
  return PARENT_POOL_SIZE+CHILD_POOL_SIZE;
}
