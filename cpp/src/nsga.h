#ifndef NSGA_H
#define NSGA_H

#include "evaluate.h"
#include "chromosome.h"

#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

struct W {
  Chromosome* c;
  int order;
  double w;
  double rank;
  double* f;
  int n;
  W(Chromosome* c, double w);
  ~W();
  bool operator<(W &b);
};

/* Multiple-objective genetic algorithm */
class MultipleObjectiveGA {
public:
  MultipleObjectiveGA(int NUM_OBJECTIVES);

  /**
   * Initializes the population.
   **/
  virtual void init_population();
  /**
   * Returns the array of chromosomes to select from.
   **/
  virtual Chromosome* select(W** pool, int length);
  /**
   * Returns the crossing of two chromosomes (not destructive).
   **/
  virtual Chromosome* cross(Chromosome &a, Chromosome &b);
  /**
   * Mutates a chromosome (not destructive).
   **/
  virtual Chromosome* mutate(Chromosome &a);

  /**
   * Returns whether a dominates b.
   *
   * a dominates b iff fitness(a) > fitness(b) for all values.
   **/
  bool dominates(Chromosome &a, Chromosome &b);

  /**
   * Returns the kth "best" chromosome, although that's actually a
   * lie.
   **/
  Chromosome* get_chromosome(int k);

  /**
   * Runs the genetic algorithm for the specified parameters.
   **/
  void run_ga();

  /**
   * Runs a single generation.
   **/
  void run_generation();

  int get_pool_size();
  
protected:
  Chromosome** pool;		// The current population
  int PARENT_POOL_SIZE;		// Parent population size
  int CHILD_POOL_SIZE;		// Child population size
  int NUM_OBJECTIVES;  // Number of objectives in the fitness function
  double** fb;
private:
  double CROSS_PROB;		// Probability of crossover
  double MUT_PROB;		// Probability of mutation
  int NUM_GENERATIONS;		// Time to run the GA for

  /**
   * Partitions the set into "fronts", where the first "front"
   * dominates those after them.
   **/
  void fast_non_dominated_sort(W** I);
  /**
   * Assigns weights to each chromosome based on their "span" of the
   * sample space of items.
   **/
  void crowding_distance_assignment(W** I, int n);
};

#endif
