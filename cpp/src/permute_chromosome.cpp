#include "permute_chromosome.h"

#include <algorithm>
#include <cstring>

#include "evaluate.h"

char* find_a(char* b, unsigned int min_k) {
  unsigned int k[A];
  char* a = new char[A];
  std::fill(&k[0], &k[A-1], 0);
  for(unsigned int i = 0; i < A; i++) {
    int p = b[i];
    a[p] = 1;
    for(unsigned int j = 0; j < alpha[p].size(); j++) {
      k[alpha[p][j]]++;
    }
    if(*std::min(&k[0], &k[A-1]) >= min_k) {
      return a;
    }
  }
  return a;
}

PermuteChromosome::PermuteChromosome(char* b, int k): Chromosome(1), b(b), length(A) {
  a = find_a(b, k);
}

double* PermuteChromosome::calc_fitness() {
  double* fit = new double;
  *fit = MAX_COST - cost(a);
  return fit;
}

unordered_map<string, PermuteChromosome*>* PermuteChromosome::chmap = new unordered_map<string, PermuteChromosome*>();

PermuteChromosome* PermuteChromosome::get_instance(char* b, int k) {
  string s = string(b, A);
  if(chmap->find(s) == chmap->end()) {
    PermuteChromosome *pc = new PermuteChromosome(b, k);
    chmap->insert(make_pair(s,pc));
  }
  return chmap->at(s);
}

void PermuteChromosome::initialize() {
  chmap->reserve(1000 * 1000);
  chmap->max_load_factor(0.8);
}

char* PermuteChromosome::get_a() {
  return a;
}

int PermuteChromosome::get_length() {
  return length;
}
