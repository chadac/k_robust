#ifndef PERMUTE_CHROMOSOME_H
#define PERMUTE_CHROMOSOME_H

#include "chromosome.h"

#include <unordered_map>
using namespace std;

class PermuteChromosome: public Chromosome {
public:
  PermuteChromosome(char* b, int k);
  double* calc_fitness();

  static PermuteChromosome* get_instance(char* a, int k);
  static void initialize();

  char* get_a();
  int get_length();
private:
  char* a;
  char* b;
  int k;
  int length;
  static unordered_map<string, PermuteChromosome*> *chmap;
};

#endif
