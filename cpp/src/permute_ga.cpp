#include "permute_ga.h"

#include "evaluate.h"
#include "permute_chromosome.h"

#include <algorithm>

PermuteGA::PermuteGA(int k): GeneticAlgorithm(), k(k) {
}

char* range(int length) {
  char* c = new char[length];
  for(int i = 0; i < length; i++) c[i] = i;
  return c;
}

void PermuteGA::init_population() {
  for(int i = 0; i < POOL_SIZE; i++) {
    char* b = range(A);
    std::random_shuffle(&b[0], &b[A]);
    pool[i] = PermuteChromosome::get_instance(b, k);
  }
}
