#ifndef PERMUTE_GA_H
#define PERMUTE_GA_H

#include "ga.h"

class PermuteGA: public GeneticAlgorithm {
public:
  PermuteGA(int k);
  
  void init_population();
  Chromosome* select(Chromosome** pool, int length);
  Chromosome* cross(Chromosome &a, Chromosome &b);
  Chromosome* mutate(Chromosome &a);
private:
  int k;
};

#endif
