#include "read_dataset.h"
#include "evaluate.h"

#include <iostream>
#include <fstream>
using namespace std;

void read_dataset(const char* filename) {
  ifstream file(filename);
  file >> A;
  file >> T;
  f = new double[A];
  for(unsigned int i = 0; i < A; i++) {
    file >> f[i];
  }
  alpha = new vector<int>[A];
  for(unsigned int i = 0; i < A; i++) {
    int m;
    file >> m;
    for(int j = 0; j < m; j++) {
      int t;
      file >> t;
      alpha[i].push_back(t);
    }
  }
  
}
