#include "select.h"


Chromosome* tournament(W** pool, int length, double SELECT_PROB) {
  int r1 = rand()%length;
  int r2 = rand()%length;
  Chromosome *c1, *c2;
  if(*pool[r1] < *pool[r2]) {
    c1 = pool[r2]->c; c2 = pool[r1]->c;
  } else {
    c1 = pool[r1]->c; c2 = pool[r2]->c;
  }

  return (rand() < SELECT_PROB ? c1:c2);
}

Chromosome* rank(W** pool, int length) {
  int n = rand() % (length*(length+1)/2);
  for(int i = 0; i < length; i++) {
    n -= length - i;
    if(n < 0) {
      return pool[i]->c;
    }
  }
  return pool[length-1]->c;
}

Chromosome* tournament(Chromosome** pool, int length, double SELECT_PROB) {
  int r1 = rand() % length;
  int r2 = rand() % length;
  Chromosome *c1, *c2;

  if(*pool[r1] < *pool[r2]) {
    c1 = pool[r2]; c2 = pool[r1];
  } else {
    c1 = pool[r1]; c2 = pool[r2];
  }

  return (rand() < SELECT_PROB ? c1:c2);
}
