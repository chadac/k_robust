#ifndef select_h
#define select_h

#include "nsga.h"
#include <stdlib.h>
using namespace std;

Chromosome* tournament(W** pool, int length, double SELECT_PROB);
Chromosome* rank(W** pool, int length);

#endif
