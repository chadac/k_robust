__author__ = 'Chad'

from sim import gen,eval
from galgorithm import GeneticAlgorithm, Chromosome
from selection import roulette, tournament
import math
import numpy as np
import numpy.random as npr
import random


def rand_chromosome(n):
    return [npr.choice([0, 1], p=[0.01, 0.99]) for i in range(n)]


CROSS_P = 0.95


def cross1(a, b):
    if random.random() > CROSS_P:
        return list(a)
    return [random.choice([i, j]) for i, j in zip(a, b)]

MUTATE_PROB = 0.05


def mutate1(a):
    return [(0 if v == 1 else 1) if random.random() < MUTATE_PROB else v for i, v in enumerate(a)]


class BoolChromosome(Chromosome):
    def __init__(self, ga, a):
        """
        Chromosome representation of problem
        :param p: Problem in tuple form (n, m, k, f, alpha)
        :param a: choice of group members
        """
        super(BoolChromosome, self).__init__()
        self.__ga = ga
        self.__n, self.__m, self.__f, self.__alpha, self.__k = ga.params()
        self._a = a

    @staticmethod
    def get_key(*args, **kwargs):
        return args[0], tuple(args[1])

    def calc_a(self):
        return list(np.where(np.array(self._a) > 0)[0])

    def calc_fitness(self):
        a = np.where(np.array(self._a) > 0)[0]
        # print len(a),self._a,a
        c = math.fsum([self.__f[i] for i in a])
        t = np.zeros((self.__m,)) + self.__k
        mcost = sum(self.__f)
        for i in a:
            t[self.__alpha[i]] -= 1
        e = sum([i for i in t if i > 0])
        # print (mcost-c) * 1. / (mci**e), eval.k_robustness(self.__n, self.__m, self.__f, self.__alpha, a)
        # x / (n^e)
        # 1 > x_m / (n)
        # n > x_m
        # n > \sqrt^e{x_m}
        if e > 2 and e > math.log(1.E9, mcost):
            return 1E-20 * mcost
        else:
            return (mcost-c+1) * 1. / (mcost**e)

    @staticmethod
    def gen_chromosome(*args, **kwargs):
        return BoolChromosome(*args, **kwargs)


class BooleanGA(GeneticAlgorithm):
    def __init__(self, n, m, k, f, alpha, **kwargs):
        self._n, self._m, self._k, self._f, self._alpha = n, m, k, f, alpha
        super(BooleanGA, self).__init__(n, m, **kwargs)

    def init_pool(self, n, m, **kwargs):
        self.pool = [self.new_chromosome(rand_chromosome(self._n))
                     for i in range(self.pool_size)]

    def select(self):
        return tournament(self.pool)

    def cross(self, a, b):
        new_a = cross1(a._a, b._a)
        return self.new_chromosome(new_a)

    def mutate(self, a):
        new_a = mutate1(a._a)
        return self.new_chromosome(new_a)

    def new_chromosome(self, *args, **kwargs):
        return BoolChromosome.add_chromosome(*((self,) + args), **kwargs)

    def params(self):
        return self._n, self._m, self._f, self._alpha, self._k


def run():
    n = 30
    m = 50
    k = 3
    f, alpha = gen.instance(n, m, k, AVG_TASKS=20)
    ga = BooleanGA(n, m, k, f, alpha, pool_size=100)

    stat_k = []
    stat_c = []
    for t in range(100):
        ga.step()
        cbest = ga.best()
        stat_k += eval.k_robustness(n, m, f, alpha, cbest)

    cbest = ga.best()
    a = [i for i, j in enumerate(cbest._a) if j]
    #print set(range(m)).difference(reduce(lambda x, y: x.union(y), [set(alpha[i]) for i in a]))
    print a
    print 'Efficient:', not not eval.efficient(n, m, f, alpha, a)
    print 'K-robustness:', eval.k_robustness(n, m, f, alpha, a)
    print 'Cost:', eval.cost(n, m, f, alpha, a)


if __name__ == '__main__':
    run()