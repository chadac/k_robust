__author__ = 'Chad'

from galgorithm import Chromosome, GeneticAlgorithm
from selection import roulette, tournament
from sim import gen, eval
import numpy as np
import numpy.random as npr
import random
import pickle


def shuffle(a):
    n = list(a)
    random.shuffle(n)
    return n

CROSS_P = 0.8


def cross_pmx(a, b):
    if random.random() > CROSS_P or a == b:
        return list(a)
    c = list(b)
    i, j = npr.choice(range(len(a)), 2)
    if j < i:
        i, j = j, i
    c[i:j] = a[i:j]
    for p in range(i, j):
        if b[p] in a[i:j]:
            continue
        pmx_fix(a, b, c, i, j, p, b[p])
    # print len(c) - len(set(c))
    return c


def pmx_fix(a, b, c, l, r, i, item):
    j = b.index(a[i])
    if l <= j and j < r:
        pmx_fix(a, b, c, l, r, j, item)
    else:
        c[j] = item

MUTATE_PROB = 0.05


def mut_swap(a):
    na = list(a)
    if random.random() < MUTATE_PROB:
        i, j = npr.choice(range(len(a)), 2)
        na[i] = a[j]
        na[j] = a[i]
    return na


class PermuteChromosome(Chromosome):
    __k_robust = dict()

    def __init__(self, ga, a):
        self.__ga = ga
        self.__n, self.__m, self.__f, self.__alpha, self.__k = ga.params()
        self._a = a
        self._a_true = None
        super(PermuteChromosome, self).__init__()

    @staticmethod
    def get_key(*args, **kwargs):
        return args[0], tuple(args[1])

    def calc_a(self):
        if self._a_true:
            return self._a_true
        if self.__ga in self.__k_robust:
            self.__k = self.__k_robust[self.__ga]

        ks = np.zeros((self.__m,), dtype='int32')
        for i, a1 in enumerate(self._a):
            ks[self.__alpha[a1]] += 1
            if min(ks) >= self.__k:
                self._a_true = self._a[:i+1]
                return self._a_true

        self.__k_robust[self.__ga] = min(ks)
        self._a_true = self._a
        return self._a_true

    def calc_fitness(self):
        a = self.calc_a()
        max_cost = sum(self.__f)
        return max_cost-eval.cost(self.__n, self.__m, self.__f, self.__alpha, a)+1

    @staticmethod
    def gen_chromosome(*args, **kwargs):
        return PermuteChromosome(*args, **kwargs)


class PermuteGA(GeneticAlgorithm):
    def __init__(self, n, m, k, f, alpha, **kwargs):
        self._n, self._m, self._k, self._f, self._alpha = n, m, k, f, alpha
        super(PermuteGA, self).__init__(**kwargs)

    def init_pool(self, *args, **kwargs):
        a = range(self._n)
        self.pool = [self.new_chromosome(shuffle(a)) for i in range(self.pool_size)]

    def select(self):
        # print 'sel'
        return tournament(self.pool)

    def cross(self, a, b):
        # print 'cross'
        ca = cross_pmx(a._a, b._a)
        return self.new_chromosome(ca)

    def mutate(self, a):
        # print 'mut'
        ma = mut_swap(a._a)
        return self.new_chromosome(ma)

    def new_chromosome(self, *args, **kwargs):
        return PermuteChromosome(*((self,) + args), **kwargs)

    def params(self):
        return self._n, self._m, self._f, self._alpha, self._k


def main():
    n = 30
    m = 50
    k = 3
    f, alpha = gen.instance(n, m, k, AVG_TASKS=20)
    ga = PermuteGA(n, m, k, f, alpha, pool_size=100)

    stat_k = []
    stat_c = []
    for t in range(100):
        ga.step()
        cbest = ga.best()
        #print cbest.calc_a()
        stat_k += [eval.k_robustness(n, m, f, alpha, cbest.calc_a())]
        stat_c += [eval.cost(n, m, f, alpha, cbest.calc_a())]

    cbest = ga.best()
    a = [i for i, j in enumerate(cbest._a) if j]
    # print set(range(m)).difference(reduce(lambda x, y: x.union(y), [set(alpha[i]) for i in a]))
    print a
    print 'Efficient:', not not eval.efficient(n, m, f, alpha, a)
    print 'K-robustness:', eval.k_robustness(n, m, f, alpha, a)
    print 'Cost:', eval.cost(n, m, f, alpha, a)

if __name__ == '__main__':
    main()
