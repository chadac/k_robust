__author__ = 'Chad'


class Chromosome(object):
    objects = dict()

    def __init__(self):
        self.__fitness = None

    def reset_cache(self):
        Chromosome.objects = dict()

    @staticmethod
    def get_key(*args, **kwargs):
        pass

    @classmethod
    def add_chromosome(cls, *args, **kwargs):
        key = cls.get_key(*args, **kwargs)
        if key in Chromosome.objects:
            return Chromosome.objects[cls, key]
        else:
            return cls.gen_chromosome(*args, **kwargs)

    def f(self):
        if not self.__fitness:
            self.__fitness = self.calc_fitness()

        return self.__fitness

    def calc_fitness(self):
        pass

    @staticmethod
    def gen_chromosome(*args, **kwargs):
        pass


class GeneticAlgorithm(object):
    def __init__(self, *args, **kwargs):
        if 'pool_size' in kwargs:
            self.pool_size = kwargs['pool_size']
        else:
            self.pool_size = 100
        self.__t = 0
        self.pool = None
        self.init_pool(*args, **kwargs)
        self.__best = None

    def step(self):
        bc = self.best()
        self.__t += 1
        # print 'time: %d' % (self.__t,)
        pool = self.select()

        npool = []
        for i, j in pool:
            npool.append(self.cross(i, j))

        for i, c in enumerate(npool):
            npool[i] = self.mutate(c)
            if not self.__best or npool[i].f() > self.__best.f():
                self.__best = npool[i]

        self.pool = npool
        self.pool[0] = bc

    def init_pool(self, *args, **kwargs):
        pass

    def select(self):
        pass

    def cross(self, a, b):
        pass

    def mutate(self, a):
        pass

    def new_chromosome(self, *args, **kwargs):
        pass

    def best(self):
        if not self.__best:
            self.__best = max(self.pool, key=lambda c: c.f())
        return self.__best

    def worst(self):
        return min(self.pool, key=lambda c: c.f())

    def avg_fitness(self):
        return sum([c.f() for c in self.pool]) * 1. / len(self.pool)
