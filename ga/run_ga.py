__author__ = 'Chad'
import ga_1
import ga_2
from sim import eval, gen
import numpy as np
import matplotlib.pyplot as plt
from optim import aort

# create optimal algorithm
# distributions over T-A distribution


def eval_c(*args):
    return (eval.k_robustness(*args),
            eval.cost(*args))


def main1():
    # alpha = pickle.load(open('../myfile.pkl', 'rb'))
    # n = len(alpha)
    # m = max([max(a) for a in alpha])
    # f = [1] * n
    # k = 3
    n = 30
    m = 50
    k = 3
    #f, alpha = gen.instance(n, m, MAX_COST=1, AVG_TASKS=20)
    f, alpha = gen.instance_uniform_cost(n, m, tasks_mu=20, tasks_sigma=5)
    print 'max k = %d' % (eval.k_robustness(n, m, f, alpha, range(n)))
    ga1 = ga_1.BooleanGA(n, m, k, f, alpha, pool_size=100)
    ga2 = ga_2.PermuteGA(n, m, k, f, alpha, pool_size=100)
    #print alpha
    ga1_s = []
    ga2_s = []
    T = 100
    for t in range(T):
        print t
        ga1.step()
        ga2.step()
        max_c1 = ga1.best()
        max_c2 = ga2.best()
        a1 = max_c1.calc_a()
        a2 = max_c2.calc_a()
        ga1_s.append(eval_c(n, m, f, alpha, a1))
        ga2_s.append(eval_c(n, m, f, alpha, a2))

    print '%5s | %8s %8s | %8s %8s' % ('Time', 'K1', 'C1', 'K2', 'C2')
    y1,y2,y3,y4 = [],[],[],[]
    for t, ((r1, c1), (r2, c2)) in enumerate(zip(ga1_s, ga2_s)):
        print '%5d | %8d %8d | %8d %8d' % (t, r1, c1, r2, c2)
        y1.append(r1), y2.append(c1), y3.append(r2), y3.append(c2)



def main2(n, m, k, max_cost, avg_tasks, T):
    n = 10
    m = 20
    k = 3
    max_cost = 100000
    T = 1
    r = []
    avg_tasks = 10 * (m * 1. / n)
    f, alpha = gen.instance(n, m, cost_dist="constant", cost_params=1, task_dist="constant", task_params=avg_tasks)
    print f
    print alpha
    # f, alpha = gen.instance_uniform_cost(n, m, tasks_mu=10, tasks_sigma=5)
    max_k = eval.k_robustness(n, m, f, alpha, range(n))
    print 'Max K =', max_k
    # print 'Calculating optimal...',
    # pfs = [a for a in aort.aort((range(n), m, f, alpha), max_cost, k)]
    # print 'Done.'
    # print '%d pareto optimal sets found.' % (len(pfs),)
    # for pf in [i for i in pfs if i[1] == 3]:
    #     print pf
    # pf = [a[0] for a in pfs]
    pf = []

    # if max_k < k:
    #     return
    for t in range(T):
        print t
        (r1, c1, po1), (r2, c2, po2) = run(n, m, k, f, alpha, pf)
        r.append([r1, c1, po1, r2, c2, po2])


    totals = [0,0,0,0]
    print '%8s %8s %5s | %8s %8s %5s' % ('K1', 'C1', 'PO1', 'K2', 'C2', 'PO2')
    for r1, c1, po1, r2, c2, po2 in r:
        totals[0] += r1
        totals[1] += c1
        totals[2] += r2
        totals[3] += c2
        print '%8d %8d %5s | %8d %8d %5s' % (r1, c1, po1, r2, c2, po2)
    for i in range(4):
        totals[i] /= 1. * T
    print '%8.2f %8.2f       | %8.2f %8.2f' % tuple(totals)

    fig = plt.figure()


def run(n, m, k, f, alpha, pf):
    ga1 = ga_1.BooleanGA(n, m, k, f, alpha, pool_size=100)
    ga2 = ga_2.PermuteGA(n, m, k, f, alpha, pool_size=100)
    for t in range(100):
        print ' ', t
        ga1.step()
        ga2.step()
    max_c1 = ga1.best()
    max_c2 = ga2.best()
    a1 = tuple(sorted(max_c1.calc_a()))
    a2 = tuple(sorted(max_c2.calc_a()))
    print '   ga1 a = %15s\n     c = %4d, k = %4d' % (str(a1), eval.cost(n, m, f, alpha, a1), eval.k_robustness(n, m, f, alpha, a1))
    print '   ga2 a = %15s\n     c = %4d, k = %4d' % (str(a2), eval.cost(n, m, f, alpha, a2), eval.k_robustness(n, m, f, alpha, a2))
    max_c1_a = eval_c(n, m, f, alpha, a1) + (a1 in pf,)
    max_c2_a = eval_c(n, m, f, alpha, a2) + (a2 in pf,)
    return max_c1_a, max_c2_a


import cProfile, pstats, StringIO


if __name__ == '__main__':
    print 'Running...'
    pr = cProfile.Profile()
    pr.enable()
    # ... do something ...
    main2(10, 20, 3, 30, 3, 5)
    pr.disable()
    s = StringIO.StringIO()
    sortby = 'cumulative'
    ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    ps.print_stats()
    print s.getvalue()
