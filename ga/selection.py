__author__ = 'Chad'
import numpy as np
import numpy.random as npr
import math


def roulette(pool):
    # print 'select'
    probs = np.array([1.*c.f() for c in pool])
    probs /= math.fsum(probs)
    return npr.choice(pool, size=(len(pool), 2), p=probs)

TOURNAMENT_P = 0.7


def tournament(pool):
    n = len(pool)
    items = npr.choice(pool, size=(n, 4))
    return [(npr.choice(sorted([x1, x2], key=lambda c: c.f()), p=[1-TOURNAMENT_P, TOURNAMENT_P]),
             npr.choice(sorted([x3, x4], key=lambda c: c.f()), p=[1-TOURNAMENT_P, TOURNAMENT_P]))
            for x1, x2, x3, x4 in items]

# mult factor = 3