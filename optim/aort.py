__author__ = 'Chad'
from sim import eval, gen
import numpy as np


def aort(tf, max_cost, k):
    pf = set()
    t = []
    solve(0, t, pf, tf, max_cost, k)
    return pf


def calc(tf, t):
    n, m, f, alpha = tf
    return tuple(t), eval.k_robustness(n, m, f, alpha, t), eval.cost(n, m, f, alpha, t)


def dominates(t1, t2):
    # print t1, t2
    r1, c1 = t1[1:]
    r2, c2 = t2[1:]
    return r1 > r2 and c1 <= c2 or r1 >= r2 and c1 < c2


def solve(n, t, pf, tf, max_cost, k):
    A, m, f, alpha = tf
    if n >= len(A):
        # print t
        t1 = calc(tf, t)
        if not t1[1]:
            return
        # if not eval.efficient(len(A), m, f, alpha, t):
        #     return
        # end if

        # print t1, pf
        d = filter(lambda t2: dominates(t2, t1), pf)
        if not d:
            d2 = filter(lambda t2: dominates(t1, t2), pf)
            pf.difference_update(d2)
            pf.add(tuple(t1))
        # end if
        return
    # end if

    a = A[n]
    t = t + [a]
    ct = eval.cost(n, m, f, alpha, t)
    kt = eval.k_robustness(n, m, f, alpha, t)

    if ct > max_cost:
        solve(n+1, t[:-1], pf, tf, max_cost, k)
        return
    # end if

    # prune those who cannot possibly be k-robust
    mina = k - kt + int(not kt)
    if len(A) - n - 1 < mina:
        solve(n+1, t[:-1], pf, tf, max_cost, k)
        return
    # end if

    # similar to mina, except that this is the maximal k-robustness with no knowledge about future items
    max_k = len(A) - n - 1 + kt
    # end if
    for t2 in pf:
        c2 = t2[2]
        k2 = t2[1]
        if c2 < ct and k2 >= max_k:
            solve(n+1, t[:-1], pf, tf, max_cost, k)
            return
        # end if
    # end for

    solve(n+1, t, pf, tf, max_cost, k)
    solve(n+1, t[:-1], pf, tf, max_cost, k)


def main():
    n = 10
    m = 11
    k = 1
    f, alpha = gen.instance_uniform_cost(n, m, tasks_mu=20, tasks_sigma=1)
    # f, alpha = gen.instance(n, m, 1000, 10)
    max_k = eval.k_robustness(n, m, f, alpha, range(n))
    print max_k
    print f, alpha
    tf = (range(n), m, f, alpha)
    pf = aort(tf, 1000, 1)
    print pf
    # a = pf.pop()
    # print a
    # print eval.k_robustness(n, m, f, alpha, a[0])


if __name__ == '__main__':
    print "Running..."
    main()
