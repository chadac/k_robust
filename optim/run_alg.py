__author__ = 'Chad'
from sim import gen,eval
from ga import ga_2


def perf(n, m, f, alpha, a):
    return a, eval.k_robustness(n, m, f, alpha, a), eval.cost(n, m, f, alpha, a)


def ga2_results(n, m, f, alpha, k):
    ga1 = ga_2.PermuteGA(n, m, k, f, alpha, pool_size=100)
    for t in range(200):
        ba, bk, bc = perf(n, m, f, alpha, ga1.best().calc_a())
        print 't: %3d c: %4d k: %4d a: %4s' % (t, bc, bk, str(ba))
        ga1.step()
    max_c1 = ga1.best()
    a1 = tuple([int(q) for q in sorted(max_c1.calc_a())])
    return perf(n, m, f, alpha, a1)


def test():
    n, m, f, alpha = gen.read_from_file('zenefa/data_50_3.json')
    print ga2_results(n, m, f, alpha, 3)


test()
