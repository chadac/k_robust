__author__ = 'Chad'
from optim import aort
from sim.eval import k_robustness, cost, efficient
from sim.gen import read_from_file
from ga import ga_1, ga_2
import json
import os, re


def perf(n, m, f, alpha, a):
    return a, k_robustness(n, m, f, alpha, a), cost(n, m, f, alpha, a)


def optimal_sets(n, m, f, k, alpha):
    tf = (range(n), m, f, alpha)
    # we dont need no max cost
    max_cost = sum(f) * 10
    pf = aort.aort(tf, max_cost, k)
    return [a[0] for a in pf]


def ga1_results(n, m, f, alpha, k):
    ga1 = ga_1.BooleanGA(n, m, k, f, alpha, pool_size=100)
    for t in range(1000):
        ga1.step()
    max_c1 = ga1.best()
    a1 = tuple([int(q) for q in sorted(max_c1.calc_a())])
    return perf(n, m, f, alpha, a1)


def ga2_results(n, m, f, alpha, k):
    ga2 = ga_2.PermuteGA(n, m, k, f, alpha, pool_size=2*n)
    for t in range(1000):
        ga2.step()
    max_c2 = ga2.best()
    a2 = tuple([int(q) for q in sorted(max_c2.calc_a())])
    return perf(n, m, f, alpha, a2)


def get_datasets(subdir):
    fs = os.listdir('datasets/%s' % (subdir,))
    ds = filter(lambda s: re.match('.*\.json', s), fs)
    return ds


def run_ga(subdir):
    # subdir = 'cc1ntk20_5n20_260_60m15_100_5'
    dss = get_datasets(subdir)
    s = 0
    if os.path.isfile('datasets/%s_results.json' % (subdir,)):
        with open('datasets/%s_results.json' % subdir, 'r') as f:
            data_obj = json.load(f)
            results = data_obj['results']
            lf = data_obj['files'][-1]
            s = dss.index(lf)
    results = []
    for dc, dsn in enumerate(dss):
        if dc < s:
            continue
        print dsn
        n, m, f, alpha = read_from_file('%s/%s' % (subdir, dsn))
        max_k = k_robustness(n, m, f, alpha, range(n))
        print '  Max k =', max_k
        for k in [3, 4, 5]:
            print ' ' * 5 + 'k =', k
            #ga1r = ga1_results(n, m, f, alpha, k)
            #print ' ' * 7 + 'ga1: k = %2d, c = %4d, a = %s' % (ga1r[1], ga1r[2], ga1r[0])
            ga2r = ga2_results(n, m, f, alpha, k)
            print ' ' * 7 + 'ga2: k = %2d, c = %4d, a = %s' % (ga2r[1], ga2r[2], ga2r[0])
            #results.append({'n': n, 'm': m, 'k': k, 'alg': 'ga1', 'file': dsn, 'r': ga1r})
            results.append({'n': n, 'm': m, 'k': k, 'alg': 'ga2', 'file': dsn, 'r': ga2r})
            with open('datasets/%s_results.json' % subdir, 'w') as fo:
                fo.write(json.dumps({'results': results, 'files': dss[:dc+1]}))

    # print ga1_pts
    # print
    # print ga2_pts
    with open('datasets/%s_results.json' % subdir, 'w') as fo:
        fo.write(json.dumps({'results': results, 'files': dss}))

if __name__ == '__main__':
    print 'Running...'
    run_ga('const_cost_normal_task')
    run_ga('normal_cost_normal_task')
    run_ga('uniform_cost_uniform_task')
    # run_ga('p2std_0.1000ut20k')
    # run_ga('p2std_0.2500ut20k')
    # run_ga('p2std_0.5000ut20k')
    # run_ga('zenefa')
    # run_ga('cc1ntk15_3')
    # run_ga('pc0.5_10ut30k')
    # run_ga('cc1ntk20_5')
    # run_ga('uc1_1000ntk20_5')
    print 'Done.'
