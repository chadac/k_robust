__author__ = 'Chad'
import json, re, os


def get_datasets(subdir):
    fs = os.listdir('datasets/%s' % (subdir,))
    ds = filter(lambda s: re.match('.*\.json', s), fs)
    return ds


def convert_file(fname):
    with open(fname, 'r') as fin:
        data = json.load(fin)
    if len(data) == 4:
        n, m, f, alpha = data
        if isinstance(n, int) and isinstance(m, int) and isinstance(f, list) and isinstance(alpha, list):
            return
    n = len(data)
    m = max([max(d) for d in data.itervalues()])+1
    f = [1] * n
    alpha = [0] * n

    for a, task_alloc in data.iteritems():
        a = int(a)
        alpha[a] = task_alloc

    with open(fname, 'w+') as file_out:
        file_out.write(json.dumps((n, m, f, alpha)))


def run():
    dss = get_datasets('zenefa')
    for dsn in dss:
        print dsn
        convert_file('datasets/zenefa/%s' % (dsn,))


def list_of_lists_to_dict(lst):
    dct = dict()
    for i, a in enumerate(lst):
        dct[i] = a
    return dct

if __name__ == '__main__':
    run()
    # print list_of_lists_to_dict([[0, 1, 2], [1, 2, 3], [2, 3, 4], [4, 5, 5]])
