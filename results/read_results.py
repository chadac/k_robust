__author__ = 'Chad'
import matplotlib.pyplot as plt
import json
import numpy as np


def plot(data_file, xkeys, ykeys, dfilters, ylegend, xlabel=None, ylabel=None, title=None):
    with open(data_file, 'r') as f:
        data = json.load(f)
        rs = data['results']
    xs = []
    ys = []
    for i, (xkey, ykey, dfilter) in enumerate(zip(xkeys, ykeys, dfilters)):
        xs.append([xkey(r)+0.1*i for r in rs if dfilter(r)])
        ys.append([ykey(r)+0.1*i for r in rs if dfilter(r)])
    ymin = min(map(min, ys))
    ymax = max(map(max, ys))

    print zip(xs[0], ys[0])

    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.ylim([ymin-1, ymax+1])
    handles = []
    colors = ['r', 'b', 'g']
    for x, y, lbl, color in zip(xs, ys, ylegend, colors[:len(xs)]):
        yl = [np.mean([y1 for x1, y1 in zip(x, y) if x1 == x2]) for x2 in x]
        handles.append(plt.scatter(x, y, marker='o', label=lbl, color=color))
        plt.plot(x, yl, label=lbl, color=color)

    plt.legend(ylegend)


def run():
    n_keys = [50, 100, 150, 200]
    k = 5
    for n in n_keys:
        print n
        xkey = lambda r: r['m']
        ykey1 = lambda r: r['r'][2]  # cost
        ykey2 = lambda r: r['r'][1]  # k-robustness
        dfilter1 = lambda r: r['n'] == n and r['alg'] == 'ga1' and r['k'] == k
        dfilter2 = lambda r: r['n'] == n and r['alg'] == 'ga2' and r['k'] == k
        legend = ['GA 1', 'GA 2']
        plt.figure("Cost")
        plot('datasets/cc1ntk20_5_results.json', [xkey] * 2, [ykey1] * 2, [dfilter1, dfilter2],
             legend, xlabel="Number of tasks", ylabel="Cost",
             title="Cost for %d agents, %d-robustness" % (n, k))
        plt.figure("K-robustness")
        plot('datasets/cc1ntk20_5_results.json', [xkey] * 2, [ykey2] * 2, [dfilter1, dfilter2],
             legend, xlabel="Number of tasks", ylabel="K-robustness",
             title="K-robustness for %d agents, %d-robustness" % (n, k))
        plt.show()


if __name__ == '__main__':
    run()