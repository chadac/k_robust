import os,re
import json


def get_datasets(subdir):
    fs = os.listdir('%s' % (subdir,))
    ds = filter(lambda s: re.match('.*\.json', s), fs)
    return ds


def convert_file(in_name, out_name):
    print in_name, out_name
    with open(in_name, 'r') as fin:
        data = json.load(fin)
        n, m, f, alpha = data
        with open(out_name, 'w') as fout:
            fout.write('%d\n' % (n,))
            fout.write('%d\n' % (m,))
            for fi in f:
                fout.write('%f ' % (fi,))
            fout.write('\n')
            for ai in alpha:
                fout.write('%d\n' % len(ai))
                for aj in ai:
                    fout.write('%d ' % aj)
                fout.write('\n')


def convert_datasets(dname):
    inpath = 'datasets/%s/' % (dname,)
    outpath = 'cpp/datasets/%s/' % (dname,)
    if not os.path.exists(outpath):
        os.mkdir(outpath)

    ds = get_datasets(inpath)
    for d in ds:
        convert_file(inpath + d, outpath + d + '.txt')


def main():
    dirs = filter(lambda x: os.path.isdir('datasets/%s' % (x,)), os.listdir('datasets'))
    print dirs
    for d in dirs:
        convert_datasets(d)

if __name__ == '__main__':
    main()