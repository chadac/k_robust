__author__ = 'Chad'
import json
import re


def transform_file(fname):
    with open('datasets/%s_results.json' % fname, 'r') as fin:
        data = json.load(fin)

    tbl = dict()
    for rs in data['results']:
        n = rs['n']
        m = rs['m']
        k = rs['k']
        fn = rs['file']
        if not re.search('\d+_%d_\d+' % (k,), fn):
            continue
        if not fn in tbl:
            tbl[fn] = [k, n, m, 0, 0, 0, 0]
        summ = [rs['r'][2], rs['r'][1]]
        if rs['alg'] == 'ga1':
            tbl[fn][3:5] = summ
        else:
            tbl[fn][5:7] = summ

    with open('datasets/%s_results.csv' % fname, 'w') as fout:
        fout.write('%5s %5s %5s %6s %6s %6s %6s\n' % ('m', 'k', 'n', 'ga1c', 'ga1k', 'ga2c', 'ga2k'))
        for k, n, m, ga1c, ga1k, ga2c, ga2k in sorted(tbl.itervalues(), key=lambda x: (x[0], x[2], x[1])):
            fout.write("%5d %5d %5d %6d %6d %6d %6d\n" % (m, k, n, ga1c, ga1k, ga2c, ga2k))

# end function

if __name__ == '__main__':
    transform_file('zenefa')
