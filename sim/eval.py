__author__ = 'Chad'
import numpy as np


def k_robustness(n, m, f, alpha, a):
    """
    Returns the k-robustness of the solution.
    :param n:
    :param m:
    :param k:
    :param f:
    :param alpha:
    :param a:
    :return:
    """
    kr = np.zeros((m,))
    for i in a:
        kr[alpha[i]] += 1

    return min(kr)


def efficient(n, m, f, alpha, a):
    """
    Returns whether the team is efficient (completes the tasks)
    :param n: Number of agents
    :param m: Number of tasks
    :param f:
    :param alpha:
    :param a:
    :return:
    """
    return k_robustness(n, m, f, alpha, a)


def cost(n, m, f, alpha, a):
    """
    Determines the cost of the team.
    :param n:
    :param m:
    :param f:
    :param alpha:
    :param a:
    :return:
    """
    return sum([f[i] for i in a])


def dominates(n, m, f, alpha, t1, t2):
    k1 = k_robustness(n, m, f, alpha, t1)
    c1 = cost(n, m, f, alpha, t1)
    k2 = k_robustness(n, m, f, alpha, t2)
    c2 = cost(n, m, f, alpha, t2)
    return k1 >= k2 and c1 > c2 or k1 > k2 and c1 >= c2