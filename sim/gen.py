__author__ = 'Chad'

import random
import numpy as np
import numpy.random as npr
import math
import os
import json
import eval
import itertools


def instance(n, m, cost_dist="uniform", cost_params=(1, 1000), task_dist="uniform", task_params=10):
    """
    Generates a new team assignment problem. This algorithm assigns agents uniformly across tasks.
    :param n: The number of agents
    :param m: The number of tasks
    :param MAX_COST: The maximum cost of hiring an agent
    :param AVG_TASKS: The desired expected number of tasks an agent can perform
    :return: f, the cost of each agent, and alpha, the tasks each agent can complete
    """
    if task_dist == "constant":
        t = task_params
        alpha = [npr.choice(range(m), size=min(t, m), replace=False) for i in range(n)]
    elif task_dist == "uniform":
        d = task_params
        A = npr.rand(n, m)
        alpha = [np.where(A[i] * m < d)[0].tolist() for i in range(n)]
        for i in range(n):
            if not len(alpha[i]):
                alpha[i] = [random.choice(range(m))]
    elif task_dist == "uniform_dist":
        lb, ub = task_params
        alpha = [list(npr.choice(range(m), size=npr.randint(lb, ub), replace=False)) for i in range(n)]
    elif task_dist == "normal":
        u, s = task_params
        alpha = [list(npr.choice(range(m), size=max(1, min(m, npr.normal(u, s))), replace=False)) for i in range(n)]
    elif task_dist == "power":
        e, d = task_params
        alpha = [npr.choice(range(m), size=min(m, math.floor(e**(d*i))), replace=False) for i in range(n)]

    if cost_dist == "constant":
        f = [1 for i in range(n)]
    elif cost_dist == "uniform":
        lb, ub = cost_params
        f = [random.randint(lb, ub) for i in range(n)]
    elif cost_dist == "normal":
        u, s = cost_params
        f = [max(1, int(npr.normal(u, s))) for i in range(n)]
    elif cost_dist == "proportional+noise":
        sc, s = cost_params
        u = int(sc * len(alpha[i]))
        f = [max(1, int(u + npr.normal(0, s * u))) for i in range(n)]

    return f, alpha


def instance_uniform_cost(n, m, tasks_mu=20, tasks_sigma=5):
    f = [1 for i in range(n)]
    A = npr.rand(n, m)
    # alpha = [np.array(np.where(A[i] < AVG_TASKS * 1. / m)[0]) for i in range(n)]
    alpha = [0] * n
    for i in range(n):
        t_size = min(m, max(1, math.floor(npr.normal(tasks_mu, tasks_sigma))))
        alpha[i] = npr.choice(range(m), size=t_size, replace=False)
    return f, alpha


def write_to_file(subdir, n, m, f, alpha, c=1):
    if not os.path.exists('datasets'):
        os.makedirs('datasets')
    if not os.path.exists('datasets/%s' % (subdir,)):
        os.makedirs('datasets/%s' % (subdir,))
    with open('datasets/%s/%05dn_%05dm_%03dt.json' % (subdir, n, m, c), 'w') as fo:
        fo.write(json.dumps((n, m, list(f), [list(a) for a in alpha])))


def read_from_file(name):
    with open('datasets/%s' % (name,)) as f:
        data = json.loads(f.read())
        n, m, f, alpha = data
        f = np.array(f)
        alpha = np.array(alpha)
        return n, m, f, alpha


def gen_datasets():
    T = 30
    n_min = 30
    n_max = 110
    n_step = 30
    m_min = 20
    m_max = 110
    m_step = 30
    for n, m, stddev, t in itertools.product(range(n_min, n_max+1, n_step),
                                             range(max(10, m_min), m_max+1, m_step),
                                             [0.2],
                                             range(T)):
    # for n,m,t in itertools.product([2000],[5000],range(20)):
        print 'n: %4d, m: %4d' % (n, m)
        print '  t: %d' % (t,)
        num_t = 18 * (m * 1. / n)
        f, alpha = instance(n, m,
                            cost_dist="uniform", cost_params=(1, 1000),
                            task_dist="uniform_dist", task_params=(1, m))
        print '   max k: %d' % (eval.k_robustness(n, m, f, alpha, range(n)),)
        write_to_file('uniform_cost_uniform_task', n, m, f, alpha, c=t)


if __name__ == '__main__':
    print 'Running...'
    gen_datasets()
    print 'Done.'
